'''
cutoff point to start counting ten thousands begin when len >= 6 and len < 9.
when len => 9, you begin using 亿.

TEST VALUES 
90204
90000
204
any multiple of 10
from 10 to 19
1001
'''
import random 

class chinese_number:
    #ordered around the Chinese system of numbers
    characters = {
            'units':['零', '一','二','三','四','五','六','七','八','九'],
            'ten':'十',
            'hundred':'百',
            'thousand':'千',
            'ten-thousand':'万',
            'hundred-million':'亿'
    }

def random_big_number():
    return random.randint(1000000, 100000000)

def obtain_attributes(chinese_number):
    attributes = [attr for attr in dir(chinese_number) if not attr.startswith('__')]
    attributes.sort(key=len)
    attributes.remove('characters')
    return attributes

def print_attributes(chinese_number):
    attributes = obtain_attributes(chinese_number)
    for attr in attributes:
        print("{}: {}".format(attr, getattr(chinese_number, attr)))

def split_number(number, exceptional):
    components = []
    multiple = 10
    while number > multiple / 10:
        component = number % multiple - number % (multiple /10)
        components.append(int(component))
        multiple *= 10
    components = remove_zeroes(number, components, exceptional)
    return components

def convert_into_chinese(number, chinese_number):
    conversion = ''
    string_number = str(number)
    left_most = int(string_number[0])

    if number >= 0 and number < 10:
        conversion += chinese_number.characters['units'][number] 
    elif number >= 10 and number < 20:
        conversion +=  chinese_number.characters['ten']
    elif number >= 20 and number < 100:
        conversion += chinese_number.characters['units'][left_most] + chinese_number.characters['ten']
    elif number >= 100 and number < 1000:
        conversion += chinese_number.characters['units'][left_most] + chinese_number.characters['hundred']
    elif number >= 1000 and number < 10000:
        conversion += chinese_number.characters['units'][left_most] + chinese_number.characters['thousand']
    elif number >= 10000 and number < 100000000:
        conversion += chinese_number.characters['units'][left_most] + chinese_number.characters['ten-thousand']
    return conversion

def obtain_chinese_numbers(numbers, chinese_number):
    chinese_numbers = []
    for number in numbers:
        zh_num = convert_into_chinese(number, chinese_number)
        chinese_numbers.append(zh_num)
    return chinese_numbers 

def fix_reverse_list(zh_nums):
    final = ''
    for item in reversed(zh_nums):
        final += item
    return final 

def check_for_repeat_zeroes(components):
    count = 0
    for index, item in enumerate(components):
        if item == 0:
            if components[index + 1] == 0:
                components = list(filter(lambda x: x != 0, components))
                break
    return components 
def remove_zeroes(number, components, exceptional):
    if number != 0 and number not in exceptional:
        if components[0] == 0:
            del components[0]
    components = check_for_repeat_zeroes(components)
    return components

def exceptional_number(number, chinese_number):
    string = ''
    if number == 0:
        string += '零'
    elif number == 1:
        string += '一'
    elif number == 10:
        string += '十'
    elif number == 100:
        string += '一百'
    elif number == 1000:
        string += '一千'
    elif number == 10000:
        string += '一万'
    elif number == 100000000:
        string += '一亿'
    return string
#experimental

exceptional = [0, 1, 10, 100, 1000, 10000, 100000000]
chinese_number = chinese_number()
number = 10000
for number in range(1000, 2001):
    if number in exceptional:
        print(exceptional_number(number, chinese_number))
    else:
        numbers = split_number(number, exceptional)
        zh_nums = obtain_chinese_numbers(numbers, chinese_number)
        #print(zh_nums)
        print("{} = {}".format(number, fix_reverse_list(zh_nums)))

